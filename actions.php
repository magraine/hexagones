<?php

$action = isset($_POST['action']) ? $_POST['action'] : null;
$data = isset($_POST['data']) ? $_POST['data'] : null;

switch($action) {
	case 'exporter';
		if ($data) {
			var_dump($data);
			file_put_contents('sauvegardes/export_'.date('Y-m-d_H-i-s').'.json', json_encode($data));
		}
		break;
	default:
		break;
}

?>
