(function($){
	$(document).ready(function(){
		
		// menu d'actions
		$('#onglets').tabs();
		$('#action-menu').accordion({ header: "h3", autoHeight: false });

		// plateau de jeu
		$('#plateau')
			.creerPlateau(10, 10, 48)
			.actionner();

		$('#terrains').creerTerrains();		
		$('#pinceaux').creerPinceaux();
		$('#constructions').creerConstructions();

		$('#btn_export').click(function(){
			_export = $('#plateau').exporter();
			$.post('actions.php', {
				action: 'exporter',
				data: _export
			});
		});
		
		$('#btn_import').click(function(){
			file = $('#file_import').val();
			_import = undefined;
			$.getJSON('sauvegardes/' + file, function(data) {
				if (data != 'undefined') {
					$('#plateau').importer(data);
				}
			});
		});


		
		
	});
})(jQuery);
