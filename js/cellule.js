(function($){

	/*
	 * Objet case qui sauve l'état d'une case
	 * et indique ses voisins proches.
	 *
	 * Usage :
	 * c = new Case(12);
	 *
	 * @param string id	Identifiant de la case
	 *
	 */	
	$.Case = function (id) {
		this.id = id;
		this.pid = 0;     // $('#case_' + id)
		this.jeu = 0;     // $('#'+id_plateau).jeu
		this.plateau = 0; // $('#'+id_plateau)
		this.terrain = 1;
		this.construction = 0;
		this.taille = 48;
		this.voisins = {
			moi:this, // alias vers moi-meme !
			nord_ouest:0,
			nord_est:0,
			ouest:0,
			est:0,
			sud_est:0,
			sud_ouest:0
		};


		/*
		 * Retourne le code HTML de la case.
		 *
		 * @param string code HTML de la case
		 */	
		this.html = function() {
			html  = "<div id='case_" + this.id + "' class='terrain'>";
			html += "	<div class='survol'>";
			html += "		<div class='construction'></div>";
			html += "	</div>";
			html += "</div>";
			return html;
		}

		/*
		 * Crée une instance jquery sur l'identifiant de la cellule.
		 * et une instance de la cellule sur l'élément jquery...
		 * Ainsi que des liens vers le plateau et l'arbre de jeu
		 */	
		this.setPid = function() {
			this.pid = $('#case_' + this.id );
			this.pid.data('case', this);
			this.pid.data('id', this.id);
			this.plateau = this.pid.parent().parent().parent();
			this.jeu = this.plateau.data('jeu');
		}


		/*
		 * Transforme une case en un terrain donné.
		 *
		 * @param int id_terrain	Identifiant du type de terrain.
		 */	
		this.definirTerrain = function(id_terrain) {
			if (id_terrain != 'undefined' && id_terrain) {
				this.terrain = parseInt(id_terrain);
				this.pid.css('background-image', 'url(img/' + this.taille + '/' + $.matrice.terrains[id_terrain].image + ')');
			}
		}

		/*
		 * Transforme une case en une construction donnée.
		 *
		 * @param int id_construction	Identifiant du type de construction.
		 */	
		this.definirConstruction = function(id_construction) {
			if (id_construction != 'undefined' && id_construction) {
				this.construction = parseInt(id_construction);
				var img = (id_construction != 0) ? 'url(img/' + this.taille + '/' + $.matrice.constructions[id_construction].image + ')' : "none";
				this.pid.find('.construction').css('background-image', img);
			}
		}

		/*
		 * Transforme les cases actives en un terrain donné.
		 *
		 * @param int id_terrain	Identifiant du type de terrain.
		 */	
		this.definirTerrains = function(id_terrain) {
			if (id_terrain != 'undefined' && id_terrain) {
				var me = this;
				this.plateau.find('.on').each(function(){
					me.jeu.cases[ $(this).data('id') ].definirTerrain(id_terrain);
				});
			}
		}

		/*
		 * Transforme les cases actives en une construction donnée.
		 *
		 * @param int id_construction	Identifiant du type de construction.
		 */	
		this.definirConstructions = function(id_construction) {
			if (id_construction != 'undefined' && id_construction) {
				var me = this;
				this.plateau.find('.on').each(function(){
					me.jeu.cases[ $(this).data('id') ].definirConstruction(id_construction);
				});
			}
		}



		/*
		 * Modifie les propriétés des cases voisines.
		 * ou de moi-même
		 *
		 * @param bool toggle	active ou desactive
		 * @param array qui		tableau des voisins a activer ['moi', 'ouest', 'est']
		 * 
		 */	
		this.hoverVoisins = function(toggle, qui) {
			if (toggle) {
				for (voisin in qui) {
					if (this.voisins[ qui[voisin] ].pid) {
						this.voisins[ qui[voisin] ].pid.addClass('on');
					}
				}
			} else {
				for (voisin in qui) {
					if (this.voisins[ qui[voisin] ].pid) {
						this.voisins[ qui[voisin] ].pid.removeClass('on');
					}
				}
			}
		}
		
	}

})(jQuery);
