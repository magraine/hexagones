(function($){

	/*
	 * Crée le HTML du plateau de jeu
	 *
	 * Usage :
	 * $('#tableau').creerPlateau(20, 20, 32);
	 *
	 * @param int largeur	Nombre de cases en largeur.
	 * @param int hauteur	Nombre de cases en hauteur.
	 * @param int taille	Taille des cases en pixels (48, 32 ou 24).
	 * @return this		Source d'appel pour chainage de fonctions.
	 *
	 */	
	$.fn.creerPlateau = function(largeur, hauteur, taille) {
		
		// largeur en nb de case
		if (typeof largeur == 'undefined') {
			largeur = 10;
		}
		// hauteur en nb de case
		if (typeof hauteur == 'undefined') {
			hauteur = 10;
		}
		// taille d'une case en px (48, 32, 24)
		if (typeof taille == 'undefined') {
			taille = 48;
		}
		// ne pas se faire avoir avec l'expression : largeur + hauteur ...
		largeur = parseInt(largeur);
		hauteur = parseInt(hauteur);
		taille  = parseInt(taille);

		plateau = $(this);
		id = plateau.attr('id');
		
		jeu = {};
		plateau.data('jeu', jeu);  // nouveau jeu.
		jeu.taille = taille;
		jeu.hauteur = hauteur;
		jeu.largeur = largeur;
		jeu.cases = {};
			
		game = plateau.append('<div id="hexa"></div>').find('#hexa');

		l = largeur * taille + (taille/2) + (largeur -1) * (taille / 6) + (taille / 12);
		h = hauteur * taille;
		game.css('width', l);
		game.css('height', h);
		game.addClass('taille_' + taille);
		plateau.css('width', l);
		plateau.css('height', h);
		
		
		// construire les cases
		for (var i = 0; i<hauteur ; i++) {

			type = (i%2==0) ? 'pair' : 'impair';
			ligne = '';
			ligne += "<div class='ligne " + type + "'>";
			
			for (var j = 1; j<=largeur; j++) {
				numero = largeur * i + j;
				
				jeu.cases[numero] = new $.Case(numero);
				jeu.cases[numero].terrain = 1;
				jeu.cases[numero].taille = taille;

				ligne += jeu.cases[numero].html();
			}
			
			ligne += "</div>";
			game.append(ligne);

		}
		
		game.find('.pair').css('padding-right', taille/2 + taille/12);
		game.find('.impair').css('padding-left', taille/2 + taille/12);
		game.find('.terrain + .terrain').css('margin-left', taille/6);

		// alias pid = $(id) et inversement
		// $('#case_3).case = la case...
		for (cellule in jeu.cases) {
			jeu.cases[cellule].setPid();
		}
		
		// definir les voisins
		for (var i = 0; i<hauteur ; i++) {
			decallage = (i%2==0) ? 0 : 1;
			for (var j = 1; j<=largeur; j++) {
				numero = largeur * i + j;
				// nord
				if (numero > largeur) {
					if (decallage || ((numero-1) % largeur)) {
						nord_ouest = numero - largeur + decallage - 1;
						jeu.cases[numero].voisins.nord_ouest = jeu.cases[nord_ouest];
					}
					if (!decallage || (numero % largeur)) {
						nord_est = numero - largeur + decallage;
						jeu.cases[numero].voisins.nord_est = jeu.cases[nord_est];
					}
				}
				// ouest
				if ((numero-1) % largeur) {
					jeu.cases[numero].voisins.ouest = jeu.cases[ numero - 1 ];
				}
				// est
				if (numero % largeur) {
					jeu.cases[numero].voisins.est = jeu.cases[ numero + 1 ];
				}
				// sud
				if (numero <= (largeur * (hauteur - 1))) {
					if (decallage || ((numero-1) % largeur)) {
						sud_ouest = numero + largeur + decallage - 1;
						jeu.cases[numero].voisins.sud_ouest = jeu.cases[sud_ouest];
					}
					if (!decallage || (numero % largeur)) {
						sud_est = numero + largeur + decallage;
						jeu.cases[numero].voisins.sud_est = jeu.cases[sud_est];
					}					
				}
			}			
		}

		return this;
	};



	var edition=false;
	$(document).mouseup(function(){
		edition = false;
	});
	
	/*
	 * Ajoute les évènements de souris possibles sur les cases du jeu
	 *
	 * Usage :
	 * $('#tableau').actionner();
	 *
	 * @return this		Source d'appel pour chainage de fonctions.
	 *
	 */	
	$.fn.actionner = function() {
		
		terrain = $('div.terrain', this);
		terrain.mousedown(function(){
			if (editerTerrain()) {
				edition = true;
				$(this).dessinerTerrain();
			}
			if (editerConstruction()) {
				edition = true;
				$(this).dessinerConstruction();
			}
		});

		terrain.hover(
			function() {
				me = $(this);
				me.soulignerVoisins();
				$.informerSurCellule( me.data('case') );
				if (edition) {
					if (editerTerrain()) {
						me.dessinerTerrain();
					}
					if (editerConstruction()) {
						me.dessinerConstruction();
					}		
				}
			},
			function() {
				me = $(this);
				me.soulignerVoisins(false);
				//$.informerSurCellule(false);
			}
		);
		return this;
	};

	/*
	 * Teste si on est en édition de terrain
	 *
	 * Usage :
	 * editerTerrain();
	 *
	 * @return bool
	 *
	 */
	function editerTerrain() {
		var edition = $('#activerTerrain:checked').val();
		if (edition) return true;
		return false;
	};
	
	/*
	 * Teste si on est en édition de construction
	 *
	 * Usage :
	 * editerTerrain();
	 *
	 * @return bool
	 *
	 */
	function editerConstruction() {
		var edition = $('#activerConstruction:checked').val();
		if (edition) return true;
		return false;
	};	 

	/*
	 * Dessine un terrain sur une case de jeu
	 * (avec un type de terrain donné par un formulaire)
	 * 
	 * @return this		Source d'appel pour chainage de fonctions.
	 *
	 */
	$.fn.dessinerTerrain = function(id_terrain) {		
		var cellule = $(this).data('case');
		var id_terrain = $('#terrains input:radio[name=terrain]:checked').val();
		cellule.definirTerrains(id_terrain);
		return this;
	}

	/*
	 * Dessine une construction sur une case de jeu
	 * (avec un type de construction donné par un formulaire)
	 * 
	 * @return this		Source d'appel pour chainage de fonctions.
	 *
	 */
	$.fn.dessinerConstruction = function(id_terrain) {		
		var cellule = $(this).data('case');
		var id_construction = $('#constructions input:radio[name=construction]:checked').val();
		cellule.definirConstructions(id_construction);
		return this;
	}

	/*
	 * Souligne les voisins de la case
	 * 
	 * @return this		Source d'appel pour chainage de fonctions.
	 *
	 */
	$.fn.soulignerVoisins = function(toggle) {

		if (typeof toggle == 'undefined') {
			toggle = true;
		}
				
		cellule = $(this).data('case');
		id_pinceau = $('#pinceaux input:radio[name=pinceau]:checked').val();
		if (!id_pinceau) { id_pinceau = 0; }
		cellule.hoverVoisins(toggle, $.matrice.pinceaux[id_pinceau].hover );
		return this;
	}



	/*
	 * Crée un export json des données du jeu.
	 *
	 * Usage :
	 * $('#plateau').exporter();
	 *
	 * @return object	Données json de l'export.
	 *
	 */
	$.fn.exporter = function() {
		var jeu = $(this).data('jeu');
		// deferencer les liaisons...
		return $.cloneJeu(jeu);
	}


	/*
	 * Charge un export json des données
	 * et reconstruit le tableau de jeu en conséquence.
	 *
	 * Usage :
	 * $('#plateau').importer( data );
	 *
	 * @param object tableau	Données (json) du tableau à reconstruire
	 * @return this		Source d'appel pour chainage de fonctions.
	 *
	 */
	$.fn.importer = function(tableau) {
		var me = $(this);
		me.empty();
		me.creerPlateau(tableau.largeur, tableau.hauteur, tableau.taille).actionner();
		var jeu = me.data('jeu');
		for (id in tableau.cases) {
			jeu.cases[id].definirTerrain( tableau.cases[id].terrain );
			jeu.cases[id].definirConstruction( tableau.cases[id].construction );
		}
		return this;
	}


	/*
	 * Cloner un jeu (le dupliquer, sans réferencer la source)
	 * 
	 * @param object obj	Objet à dupliquer
	 * @return object Objet dupliqué.
	 *
	 */
	$.cloneJeu = function(obj){
		if(obj == null || typeof(obj) != 'object')
			return obj;

		var temp = {};

		for(var key in obj) {
			if (
				(key == 'voisins') ||
				(key == 'plateau') ||
				(key == 'pid') ||
				(key == 'jeu') ||
				(key == 'html') 
			) {
				continue;
			}
			if (typeof( obj[key] ) == 'function') {
				continue;
			}
			temp[key] = $.cloneJeu(obj[key]);
			
		}
		return temp;
	}

})(jQuery);
