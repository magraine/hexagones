(function($){

	/*
	 * Crée une case radio pour selectionner
	 * un type d'image (élément de terrain, de construction...)
	 *
	 * @param string type 	Le nom du type (terrain, pinceau, construction)
	 * @param int id		L'identifiant du type voulu
	 * @param int taille	La taille en pixel de l'image souhaitée (48, 32 ou 24)
	 *
	 * @return string 	Le code HTML correspondant.
	 */
	$.selecteurImage = function(type, id, taille) {
		
		// taille d'une case en px (48, 32, 24)
		if (typeof taille == 'undefined') {
			taille = 48;
		}

		var ident = type + '_' + id;
		var img = "img/" + taille + "/" + $.matrice[ $.matrice.pluriel[type] ][id].image;
		var html  = "<div class='element'>\n";
		    html += "	<label for='" + ident+ "'><img src='" + img + "' width='" + taille + "px' height='" + taille + "px' /></label>\n";
		    html += "	<input id='" + ident + "' type='radio' name='" + type + "' value='" +  id + "' />\n";
		    html += "</div>\n";
		return html;
	}

	/*
	 * Affiche la liste des terrains utilisables
	 * à l'emplacement spécifié
	 *
	 * @param int taille	La taille en pixel des images souhaitée (48, 32 ou 24)
	 */
	$.fn.creerTerrains = function(taille) {
		var me = $(this);
		for (var id in $.matrice.terrains){
			me.append( $.selecteurImage('terrain', id, taille) );
		}
	}

	/*
	 * Affiche la liste des pinceaux utilisables
	 * à l'emplacement spécifié
	 *
	 * @param int taille	La taille en pixel des images souhaitée (48, 32 ou 24)
	 */
	$.fn.creerPinceaux = function(taille) {
		var me = $(this);
		for (var id in $.matrice.pinceaux){
			me.append( $.selecteurImage('pinceau', id, taille) );
		}
	}

	/*
	 * Affiche la liste des constructions utilisables
	 * à l'emplacement spécifié
	 *
	 * @param int taille	La taille en pixel des images souhaitée (48, 32 ou 24)
	 */
	$.fn.creerConstructions = function(taille) {
		var me = $(this);
		for (var id in $.matrice.constructions){
			me.append( $.selecteurImage('construction', id, taille) );
		}
	}


	/*
	 * Affiche les informations concernant la cellule designee
	 */
	$.informerSurCellule = function(cellule){
		if (cellule) {
			var html = "<dl>\n";
			
				// infos de la case
			    html += "	<dt>Cellule</dt>\n";
			    html += "	<dd>Numéro: " + cellule.id + "</dd>\n";

			    // infos de terrain
			    html += "	<dt>Terrain</dt>\n";
			    html += "	<dd>" + $.matrice.terrains[ cellule.terrain ].nom + " (" + cellule.terrain + ")</dd>\n";

			    // infos de construction
			    if (cellule.construction) {
					html += "	<dt>Construction</dt>\n";
					html += "	<dd>" + $.matrice.constructions[ cellule.construction ].nom + " (" + cellule.construction + ")</dd>\n";
				}
			    
			    html += "</dt>\n";
			$('#informations').html(html);
		} else {
			$('#informations').html('');
		}
	};

	/*
	 * Source des images et informations d'actions.
	 */
	$.matrice = {
		"pluriel": {
			"terrain":"terrains",
			"pinceau":"pinceaux",
			"construction":"constructions"
		},
		
		"terrains": {
			0:{	// 
				"nom":"Route",
				"image":"terrains/gris_fonce.png"
			},
			1:{ // route
				"nom":"Rocher",
				"image":"terrains/grey.png"
			},
			2:{ // zone cotière, rivière
				"nom":"Eau peu profonde",
				"image":"terrains/cyan.png"
			},	
			3:{	// mer infranchissable
				"nom":"Eau profonde",
				"image":"terrains/blue.png"
			},
			4:{	// foret
				"nom":"Forêt",
				"image":"terrains/green.png"
			},
			5:{	// prairie
				"nom":"Prairie",
				"image":"terrains/pomme.png"
			},
			6:{	// desert
				"nom":'Désert',
				"image":"terrains/yellow.png"
			},
			7:{	// 
				"nom":"Terre cultivée",
				"image":"terrains/orange.png"
			},
			8:{	// 
				"nom":"Lave",
				"image":"terrains/red.png"
			},
			9:{	// terre
				"nom":"Terre non cultivée",
				"image":"terrains/brown.png"
			},
		},
		
		"pinceaux": {
			0:{	// 1 case
				"nom":"un",
				"image":"pinceaux/un.png",
				"hover":["moi"]
			},
			1:{ // 3 cases
				"nom":"trois",
				"image":"pinceaux/trois.png",
				"hover":["moi", "nord_ouest", "nord_est"]
			},
			2:{	// 7 cases
				"nom":"sept",
				"image":"pinceaux/sept.png",
				"hover":["moi", "nord_ouest", "nord_est", "ouest", "est", "sud_ouest", "sud_est"]
			},
		},

		"constructions": {
			0:{	
				"nom":"Gomme",
				"image":"constructions/gomme.png"
			},
			1:{	
				"nom":"Maison",
				"image":"constructions/maison.png"
			},
			2:{ 
				"nom":"Lieu d'arrivée",
				"image":"flag_green.png"
			},
		}
	}

	
})(jQuery);

